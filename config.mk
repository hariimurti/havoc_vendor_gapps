ifeq ($(TARGET_GAPPS_ARCH),)
$(error "GAPPS: TARGET_GAPPS_ARCH is undefined")
endif

ifneq ($(TARGET_GAPPS_ARCH),arm)
ifneq ($(TARGET_GAPPS_ARCH),arm64)
$(error "GAPPS: Only arm and arm64 are allowed")
endif
endif

$(call inherit-product, vendor/gapps/common-blobs.mk)

# Include package overlays
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += vendor/gapps/overlay
DEVICE_PACKAGE_OVERLAYS += vendor/gapps/overlay/common/

# SetupWizard
PRODUCT_PRODUCT_PROPERTIES += \
    ro.wallpapers_loc_request_suw=true \
    setupwizard.enable_assist_gesture_training=true \
    setupwizard.feature.baseline_setupwizard_enabled=true \
    setupwizard.feature.show_pixel_tos=true \
    setupwizard.feature.show_support_link_in_deferred_setup=false \
    setupwizard.theme=glif_v3_light

# SetupWizard overlay
PRODUCT_PACKAGES += \
    PixelSetupWizardOverlay

# Gestures
PRODUCT_PROPERTY_OVERRIDES += \
    ro.boot.vendor.overlay.theme=com.android.internal.systemui.navbar.gestural

# IME
PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.ime.bs_theme=true \
    ro.com.google.ime.system_lm_dir=/system/product/usr/share/ime/google/d3_lms \
    ro.com.google.ime.theme_id=5

# framework
PRODUCT_PACKAGES += \
    com.google.android.dialer.support \
    com.google.android.maps

ifeq ($(TARGET_INCLUDE_STOCK_ARCORE),true)
PRODUCT_PACKAGES += \
    arcore
endif

# System app
PRODUCT_PACKAGES += \
    GoogleExtShared \
    GooglePrintRecommendationService

# System priv-app
PRODUCT_PACKAGES += \
    GoogleExtServicesPrebuilt

# Product app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    GoogleContacts \
    GoogleContactsSyncAdapter \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle

# Product priv-app
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    CarrierServices \
    ConfigUpdater \
    ConnMetrics \
    GoogleDialer \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleServicesFramework \
    Phonesky \
    PrebuiltGmsCoreQt \
    SafetyHubPrebuilt \
    SettingsIntelligenceGooglePrebuilt \
    SetupWizardPrebuilt

ifneq ($(TARGET_GAPPS_LITE),true)
PRODUCT_PACKAGES += \
    Chrome \
    GoogleTTS \
    LatinIMEGooglePrebuilt \
    LocationHistoryPrebuilt \
    MarkupGoogle \
    MatchmakerPrebuilt \
    NexusWallpapersStubPrebuilt2019Static \
    Photos \
    PixelSetupWizard \
    talkback \
    TipsPrebuilt \
    TurboPrebuilt \
    Velvet \
    WebViewGoogle \
    WellbeingPrebuilt

ifneq ($(TARGET_SUPPORTS_GOOGLE_RECORDER),false)
PRODUCT_PACKAGES += \
    NgaResources
endif
endif

# Recorder
ifneq ($(TARGET_SUPPORTS_GOOGLE_RECORDER),false)
PRODUCT_PACKAGES += \
    RecorderPrebuilt
endif

# Wallpapers
ifneq ($(TARGET_SUPPORTS_PIXEL_WALLPAPER),false)
PRODUCT_PACKAGES += \
    PixelLiveWallpaperPrebuilt
endif
